package com.example.xoaivazov

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    lateinit var NextButton : Button
    lateinit var Rules : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome_players)
        NextButton = findViewById(R.id.next_btn)
        Rules = findViewById(R.id.game_rules)
        val player_1 = findViewById<EditText>(R.id.player_1)
        val player_2 = findViewById<EditText>(R.id.player_2)

        NextButton.setOnClickListener(){
            val player1_1 : String = player_1.text.toString()
            val player2_2 : String = player_2.text.toString()
            val intent = Intent(this, SecondActivity::class.java)
            intent.putExtra("player1", player1_1)
            intent.putExtra("player2", player2_2)
            startActivity(intent)

        }
        Rules.setOnClickListener(){
            Toast.makeText(applicationContext, "ამ თამაშის წესები მარტივია. თამაშბს ორი მოთამაშე," +
                    " ვინც პირველი დააწყობს ერთ რიგში/სვეტში/დიაგონალში 3 კრესტიკს ან ნოლიკს ის იგებს."
                , Toast.LENGTH_LONG).show()
        }
    }
}