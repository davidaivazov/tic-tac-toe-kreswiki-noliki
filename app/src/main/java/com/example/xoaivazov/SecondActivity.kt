package com.example.xoaivazov

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
class SecondActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var Btn1: Button
    private lateinit var Btn2: Button
    private lateinit var Btn3: Button
    private lateinit var Btn4: Button
    private lateinit var Btn5: Button
    private lateinit var Btn6: Button
    private lateinit var Btn7: Button
    private lateinit var Btn8: Button
    private lateinit var Btn9: Button
    private lateinit var BtnReset: Button
    private lateinit var BtnRules: Button
    private lateinit var BtnMenu : Button
    private lateinit var player_11: TextView
    private lateinit var player_22: TextView
    private lateinit var Win1: TextView
    private lateinit var Win2: TextView
    private var activePlayer = 1
    private var firstPlayer = ArrayList<Int>()
    private var secondPlayer = ArrayList<Int>()
    private var first_win = 0
    private var second_win = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        BtnMenu = findViewById(R.id.menu_btn)
        BtnMenu.setOnClickListener() {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        Win1 = findViewById(R.id.win_1)
        Win2 = findViewById(R.id.win_2)
        Win1.text = first_win.toString()
        Win2.text = second_win.toString()
        player_11 = findViewById(R.id.player_11)
        player_22 = findViewById(R.id.player_22)
        val player1_1 = intent?.extras?.getString("player1", "Player One")
        val player2_2 = intent?.extras?.getString("player2", "Player Two")
        player_11.text = player1_1
        player_22.text = player2_2
        init()
        Toast.makeText(applicationContext, "X-ის სვლა", Toast.LENGTH_SHORT).show()
        BtnRules.setOnClickListener(){
            Toast.makeText(applicationContext, "ამ თამაშის წესები მარტივია. თამაშბს" +
                    " ორი მოთამაშე, ვინც პირველი დააწყობს ერთ რიგში/სვეტში/დიაგონალში " +
                    "3 კრესტიკს ან ნოლიკს ის იგებს."
                , Toast.LENGTH_LONG).show()
        }
        BtnReset.setOnClickListener(){
            reset()}
    }
    fun reset(){
        firstPlayer.clear()
        secondPlayer.clear()

        Btn1.text = ""
        Btn1.setBackgroundColor(Color.argb(17,255,255,255))
        Btn1.isEnabled = true
        Btn2.text = ""
        Btn2.setBackgroundColor(Color.argb(17,255,255,255))
        Btn2.isEnabled = true
        Btn3.text = ""
        Btn3.setBackgroundColor(Color.argb(17,255,255,255))
        Btn3.isEnabled = true
        Btn4.text = ""
        Btn4.setBackgroundColor(Color.argb(17,255,255,255))
        Btn4.isEnabled = true
        Btn5.text = ""
        Btn5.setBackgroundColor(Color.argb(17,255,255,255))
        Btn5.isEnabled = true
        Btn6.text = ""
        Btn6.setBackgroundColor(Color.argb(17,255,255,255))
        Btn6.isEnabled = true
        Btn7.text = ""
        Btn7.setBackgroundColor(Color.argb(17,255,255,255))
        Btn7.isEnabled = true
        Btn8.text = ""
        Btn8.setBackgroundColor(Color.argb(17,255,255,255))
        Btn8.isEnabled = true
        Btn9.text = ""
        Btn9.setBackgroundColor(Color.argb(17,255,255,255))
        Btn9.isEnabled = true

        Toast.makeText(applicationContext, "X-ის სვლა", Toast.LENGTH_SHORT).show()
        activePlayer = 1

    }

    private fun init(){
        Btn1 = findViewById(R.id.btn1)
        Btn2 = findViewById(R.id.btn2)
        Btn3 = findViewById(R.id.btn3)
        Btn4 = findViewById(R.id.btn4)
        Btn5 = findViewById(R.id.btn5)
        Btn6 = findViewById(R.id.btn6)
        Btn7 = findViewById(R.id.btn7)
        Btn8 = findViewById(R.id.btn8)
        Btn9 = findViewById(R.id.btn9)
        BtnReset = findViewById(R.id.reset)
        BtnRules = findViewById(R.id.rules)
        Btn1.setOnClickListener(this)
        Btn2.setOnClickListener(this)
        Btn3.setOnClickListener(this)
        Btn4.setOnClickListener(this)
        Btn5.setOnClickListener(this)
        Btn6.setOnClickListener(this)
        Btn7.setOnClickListener(this)
        Btn8.setOnClickListener(this)
        Btn9.setOnClickListener(this)
        BtnReset.setOnClickListener(this)
        BtnRules.setOnClickListener(this)

    }
    override fun onClick(clickedView: View?) {

        if (clickedView is Button){
            var buttonNumber = 0
            when(clickedView.id){
                R.id.btn1 -> buttonNumber = 1
                R.id.btn2 -> buttonNumber = 2
                R.id.btn3 -> buttonNumber = 3
                R.id.btn4 -> buttonNumber = 4
                R.id.btn5 -> buttonNumber = 5
                R.id.btn6 -> buttonNumber = 6
                R.id.btn7 -> buttonNumber = 7
                R.id.btn8 -> buttonNumber = 8
                R.id.btn9 -> buttonNumber = 9

            }
            if (buttonNumber != 0) {
                playGame(clickedView, buttonNumber)
            }
        }
    }
    private fun playGame(clickedView: Button, buttonNumber: Int) {

        if (activePlayer == 1){
            clickedView.text = "X"
            clickedView.setBackgroundColor(Color.GREEN)
            firstPlayer.add(buttonNumber)
            activePlayer = 2
        } else{
            clickedView.text = "O"
            clickedView.setBackgroundColor(Color.YELLOW)
            secondPlayer.add(buttonNumber)
            activePlayer = 1

        }
        clickedView.isEnabled = false
        check()

    }
    private fun check(){
        var winner= 0

        if (firstPlayer.contains(1) && firstPlayer.contains(2) && firstPlayer.contains(3)){
            winner = 1
            first_win = first_win + 1
            Win1.text = first_win.toString()
        }
        else if (secondPlayer.contains(1) and secondPlayer.contains(2) and secondPlayer.contains(3)){
            winner = 2
            second_win = second_win +1
            Win2.text = second_win.toString() }
        else if (firstPlayer.contains(4) and firstPlayer.contains(5) and firstPlayer.contains(6)){
            winner = 1
            first_win = first_win + 1
            Win1.text = first_win.toString() }
        else if (secondPlayer.contains(4) and secondPlayer.contains(5) and secondPlayer.contains(6)){
            winner = 2
            second_win = second_win +1
            Win2.text = second_win.toString()}
        else if (firstPlayer.contains(7) and firstPlayer.contains(8) and firstPlayer.contains(9)){
            winner = 1
            first_win = first_win + 1
            Win1.text = first_win.toString()}
        else if (secondPlayer.contains(7) and secondPlayer.contains(8) and secondPlayer.contains(9)){
            winner = 2
            second_win = second_win +1
            Win2.text = second_win.toString()}
        else if (firstPlayer.contains(1) and firstPlayer.contains(5) and firstPlayer.contains(9)){
            winner = 1
            first_win = first_win + 1
            Win1.text = first_win.toString()}
        else if (secondPlayer.contains(1) and secondPlayer.contains(5) and secondPlayer.contains(9)){
            winner = 2
            second_win = second_win +1
            Win2.text = second_win.toString()}
        else if (firstPlayer.contains(3) and firstPlayer.contains(5) and firstPlayer.contains(7)){
            winner = 1
            first_win = first_win + 1
            Win1.text = first_win.toString()}
        else if (secondPlayer.contains(3) and secondPlayer.contains(5) and secondPlayer.contains(7)){
            winner = 2
            second_win = second_win +1
            Win2.text = second_win.toString()}
        else if (firstPlayer.contains(1) and firstPlayer.contains(4) and firstPlayer.contains(7)){
            winner = 1
            first_win = first_win + 1
            Win1.text = first_win.toString()}
        else if (secondPlayer.contains(1) and secondPlayer.contains(4) and secondPlayer.contains(7)){
            winner = 2
            second_win = second_win +1
            Win2.text = second_win.toString()}
        else if (firstPlayer.contains(2) and firstPlayer.contains(5) and firstPlayer.contains(8)) {
            winner = 1
            first_win = first_win + 1
            Win1.text = first_win.toString()}
        else if (secondPlayer.contains(2) and secondPlayer.contains(5) and secondPlayer.contains(8)){
            winner = 2
            second_win = second_win +1
            Win2.text = second_win.toString()}
        else if (firstPlayer.contains(3) and firstPlayer.contains(6) and firstPlayer.contains(9)){
            winner = 1
            first_win = first_win + 1
            Win1.text = first_win.toString()}
        else if (secondPlayer.contains(3) and secondPlayer.contains(6) and secondPlayer.contains(9)){
            winner = 2
            second_win = second_win +1
            Win2.text = second_win.toString()}
        else if (Btn1.text.isNotEmpty() and Btn2.text.isNotEmpty() and Btn3.text.isNotEmpty() and
            Btn4.text.isNotEmpty() and Btn5.text.isNotEmpty()  and Btn6.text.isNotEmpty() and
            Btn7.text.isNotEmpty() and Btn8.text.isNotEmpty() and Btn9.text.isNotEmpty() and
            (winner == 0)){
            Toast.makeText(this, "გამარჯვებული არ არის!!! ", Toast.LENGTH_LONG).show()
            reset()
        }

        if (winner == 1){
            Toast.makeText(this, "მოიგეს X-ბმა!!!(მოთამაშე: ${player_11.text})", Toast.LENGTH_LONG).show()
            reset()
        } else if (winner == 2){
            Toast.makeText(this, "მოიგეს 0-ბმა!!!(მოთამაშე: ${player_22.text})", Toast.LENGTH_LONG).show()
            reset()
        }
    }

}


